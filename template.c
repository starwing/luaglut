/*
** lglut.c
** vim: set sw=2 et:
*/

#if defined(USE_OPENGLUT)
# include <GL/openglut.h>
# include <GL/openglut_exp.h>
#elif defined(USE_OPENGLEAN)
# include <GL/openglean.h>
# include <GL/openglean_exp.h>
#elif defined(USE_FREEGLUT)
# include <GL/freeglut.h>
#elif defined(__APPLE__) && defined(__MACH__)
# include <GLUT/glut.h>
#else
# include <GL/glut.h>
#endif


#define LUA_LIB
#include <lua.h>
#include <lauxlib.h>

#define VERSION "0.1"


static lua_State *ref_L = NULL;
static int idle_func_ref;

#define WINTABLE   (void*)0xE127AB1F
#define MENUTABLE  (void*)0xEE2077AB
#define TIMERTABLE (void*)0x71EE77AB

static void init_tables(lua_State *L) {
  lua_newtable(L);
  lua_rawsetp(L, LUA_REGISTRYINDEX, WINTABLE);
  lua_newtable(L);
  lua_rawsetp(L, LUA_REGISTRYINDEX, MENUTABLE);
  lua_newtable(L);
  lua_rawsetp(L, LUA_REGISTRYINDEX, TIMERTABLE);
}

static int get_wintable(lua_State *L, int id) {
  lua_rawgetp(L, LUA_REGISTRYINDEX, WINTABLE);
  if (id > 0) {
    lua_rawgeti(L, -1, id);
    if (lua_isnil(L, -1)) {
      lua_pop(L, 2);
      return 0;
    }
    lua_remove(L, -2);
    return 1;
  }
  return luaL_error(L, "no active glut window (id = %d)", id);
}

static int new_wintable(lua_State *L, int id, int parent) {
  if (id > 0) {
    lua_newtable(L);
    lua_rawgetp(L, LUA_REGISTRYINDEX, WINTABLE);
    lua_pushvalue(L, -2); /* wintable */
    lua_rawseti(L, -2, id);
    if (parent > 0) {
      lua_rawgeti(L, -1, parent);
      if (!lua_isnil(L, -1)) {
        int len = lua_rawlen(L, -1);
        lua_pushinteger(L, id);
        lua_rawseti(L, -1, len + 1);
      }
      lua_pop(L, 1);
    }
    lua_pop(L, 1);
    return 1;
  }
  return luaL_error(L, "invalid glut window id (id = %d)", id);
}

static void del_wintable(lua_State *L, int id) {
  lua_rawgetp(L, LUA_REGISTRYINDEX, WINTABLE);
  lua_pushnil(L);
  lua_rawseti(L, -2, id);
}

static int traceback(lua_State *L) {
  const char *msg = lua_tostring(L, 1);
  if (msg)
    luaL_traceback(L, L, msg, 1);
  else if (!lua_isnoneornil(L, 1)) {  /* is there an error object? */
    if (!luaL_callmeta(L, 1, "__tostring"))  /* try its 'tostring' metamethod */
      lua_pushliteral(L, "(no error message)");
  }
  return 1;
}

static int safe_call(lua_State *L, int nargs, int nrets) {
  int tbidx = lua_absindex(L, -nargs-2);
  lua_pushcfunction(L, traceback);
  lua_insert(L, tbidx);
  int res = lua_pcall(L, nargs, nrets, tbidx);
  lua_remove(L, tbidx);
  return res == LUA_OK;
}

static int callmethod(lua_State *L, const char *name, int nargs, int nrets) {
  get_wintable(L, glutGetWindow());
  lua_getfield(L, -1, name);
  if (lua_isnil(L, -1)) {
    lua_pop(L, 2);
    return 1;
  }
  /* arg1 arg2 ... argn wintable func */
  lua_insert(L, -nargs-2);
  lua_insert(L, -nargs-2);
  if (!safe_call(L, nargs, nrets)) {
    fprintf(stderr, "%s: %s\n", name, lua_tostring(L, -1));
    lua_pop(L, 1);
    lua_pushnil(L);
    lua_setfield(L, -2, name);
    lua_pop(L, 1);
    return 0;
  }
  /* wintable ret1 ret2 ... retn */
  lua_remove(L, -nrets-1);
  return 1;
}

#define define_callback_setter(name, glutname)                 \
  static int Lglut_##glutname##Func(lua_State *L) {     \
    int isnil = lua_isnoneornil(L, 1);                  \
    if (!isnil) luaL_checktype(L, 1, LUA_TFUNCTION);    \
    get_wintable(L, glutGetWindow());                   \
    lua_pushvalue(L, 1);                                \
    lua_setfield(L, -2, #name);                         \
    glut##glutname##Func(isnil ? NULL : name##_func);   \
    return 0;                                           \
  }

/* begin_spec(Init) */
static int Lglut_Init(lua_State *L) {
  if (lua_gettop(L) == 0) {
    int argc = 0;
    glutInit(&argc, NULL);
    return 0;
  }
  else {
    int i, argc = lua_rawlen(L, 1);
    const char **argv = (const char**)lua_newuserdata(L, sizeof(char*)*(argc+1));
    luaL_checktype(L, 1, LUA_TTABLE);
    for (i = 0; i < argc; ++i) {
      lua_rawgeti(L, 1, i+1);
      if (lua_isstring(L, -1)) {
        lua_pushfstring(L, "argument #%d is not string", i+1);
        luaL_argerror(L, 1, lua_tostring(L, -1));
      }
      argv[i] = lua_tostring(L, -1);
      lua_pop(L, 1);
    }
    argv[argc-1] = NULL;
    glutInit(&argc, (char**)argv);
  }
  return 0;
}

/* begin_spec(CreateWindow) */
static int Lglut_CreateWindow(lua_State *L) {
  const char *name = luaL_checkstring(L, 1);
  int id = glutCreateWindow(name);
  new_wintable(L, id, 0);
  lua_pushinteger(L, (lua_Integer)id);
  return 1;
}

/* begin_spec(CreateSubWindow) */
static int Lglut_CreateSubWindow(lua_State *L) {
  int win    = luaL_checkint(L, 1);
  int x      = luaL_checkint(L, 2);
  int y      = luaL_checkint(L, 3);
  int width  = luaL_checkint(L, 4);
  int height = luaL_checkint(L, 5);
  int id = glutCreateSubWindow(win, x, y, width, height);
  new_wintable(L, id, win);
  lua_pushinteger(L, (lua_Integer)id);
  return 1;
}

/* begin_spec(CreateMenuWindow) */
static int Lglut_CreateMenuWindow(lua_State *L) {
  int win    = luaL_checkint(L, 1);
  int x      = luaL_checkint(L, 2);
  int y      = luaL_checkint(L, 3);
  int width  = luaL_checkint(L, 4);
  int height = luaL_checkint(L, 5);
  int id = glutCreateMenuWindow(win, x, y, width, height);
  new_wintable(L, id, win);
  lua_pushinteger(L, (lua_Integer)id);
  return 1;
}

/* begin_spec(DestroyWindow) */
static int Lglut_DestroyWindow(lua_State *L) {
  int id = luaL_checkint(L, 1);
  glutDestroyWindow(id);
  del_wintable(L, id);
  return 0;
}

/* begin_spec(CreateMenu) */
static void menu_func(int value) {
  lua_State *L = ref_L;
  lua_getfield(L, LUA_REGISTRYINDEX, MENUTABLE);
  lua_rawgeti(L, -1, value);
  if (lua_isnil(L, -1)) {
    lua_pop(L, 2);
    return;
  }
  lua_pushinteger(L, (lua_Integer)value);
  if (!safe_call(L, 1, 0)) {
    lua_pushnil(L);
    lua_rawseti(L, -2, value);
  }
  lua_pop(L, 1);
}

static int Lglut_CreateMenu(lua_State *L) {
  int id;
  luaL_checktype(L, 1, LUA_TFUNCTION);
  id = glutCreateMenu(menu_func);
  lua_getfield(L, LUA_REGISTRYINDEX, MENUTABLE);
  lua_pushvalue(L, 1);
  lua_rawseti(L, -2, id);
  lua_pushinteger(L, (lua_Integer)id);
  return 1;
}

/* begin_spec(DestroyMenu) */

static int Lglut_DestroyMenu(lua_State *L) {
  int id = luaL_checkint(L, 1);
  lua_getfield(L, LUA_REGISTRYINDEX, MENUTABLE);
  lua_pushnil(L);
  lua_rawseti(L, -2, id);
  return 0;
}

/* begin_spec(DisplayFunc) */
static void display_func(void) {
  callmethod(ref_L, "display", 0, 0);
}

define_callback_setter(display, Display)

/* begin_spec(OverlayDisplayFunc) */
static void overlay_display_func(void) {
  callmethod(ref_L, "overlay_display", 0, 0);
}

define_callback_setter(overlay_display, OverlayDisplay)

/* begin_spec(ReshapeFunc) */
static void reshape_func(int width, int height) {
  lua_State *L = ref_L;
  lua_pushinteger(L, (lua_Integer)width);
  lua_pushinteger(L, (lua_Integer)height);
  callmethod(L, "reshape", 2, 0);
}

define_callback_setter(reshape, Reshape)

/* begin_spec(KeyboardFunc) */
static void keyboard_func(unsigned char ch, int x, int y) {
  lua_State *L = ref_L;
  lua_pushinteger(L, (lua_Integer)ch);
  lua_pushinteger(L, (lua_Integer)x);
  lua_pushinteger(L, (lua_Integer)y);
  callmethod(L, "keyboard", 3, 0);
}

define_callback_setter(keyboard, Keyboard)

/* begin_spec(MouseFunc) */
static void mouse_func(int button, int state, int x, int y) {
  lua_State *L = ref_L;
  lua_pushinteger(L, (lua_Integer)button);
  lua_pushinteger(L, (lua_Integer)state);
  lua_pushinteger(L, (lua_Integer)x);
  lua_pushinteger(L, (lua_Integer)y);
  callmethod(L, "mouse", 4, 0);
}

define_callback_setter(mouse, Mouse)

/* begin_spec(MotionFunc) */
static void motion_func(int x, int y) {
  lua_State *L = ref_L;
  lua_pushinteger(L, (lua_Integer)x);
  lua_pushinteger(L, (lua_Integer)y);
  callmethod(L, "motion", 2, 0);
}

define_callback_setter(motion, Motion)

/* begin_spec(PassiveMotionFunc) */
static void passive_motion_func(int x, int y) {
  lua_State *L = ref_L;
  lua_pushinteger(L, (lua_Integer)x);
  lua_pushinteger(L, (lua_Integer)y);
  callmethod(L, "passive_motion", 2, 0);
}

define_callback_setter(passive_motion, PassiveMotion)

/* begin_spec(SpecialFunc) */
static void special_func(int key, int x, int y) {
  lua_State *L = ref_L;
  lua_pushinteger(L, (lua_Integer)key);
  lua_pushinteger(L, (lua_Integer)x);
  lua_pushinteger(L, (lua_Integer)y);
  callmethod(L, "special", 3, 0);
}

define_callback_setter(special, Special)

/* begin_spec(IdleFunc) */
static void idle_func(void) {
  lua_State *L = ref_L;
  lua_rawgeti(L, LUA_REGISTRYINDEX, idle_func_ref);
  lua_call(L, 0, 0);
}

static int Lglut_IdleFunc(lua_State *L) {
  if (lua_gettop(L) == 0) {
    idle_func_ref = 0;
    glutIdleFunc(NULL);
    return 0;
  }
  luaL_checktype(L, 1, LUA_TFUNCTION);
  if (idle_func_ref != 0)
    luaL_unref(L, LUA_REGISTRYINDEX, idle_func_ref);
  lua_pushvalue(L, 1);
  idle_func_ref = luaL_ref(L, LUA_REGISTRYINDEX);
  glutIdleFunc(idle_func);
  return 0;
}

/* begin_spec(TimerFunc) */
static void timer_func(int index) {
  lua_State *L = ref_L;
  lua_rawgetp(L, LUA_REGISTRYINDEX, TIMERTABLE);
  lua_rawgeti(L, -1, index);
  if (lua_isnil(L, -1)) {
    lua_pop(L, 2);
    return;
  }
  lua_pushinteger(L, index);
  lua_call(L, 1, 1);
  if (lua_isnumber(L, -1))
    glutTimerFunc((int)lua_tointeger(L, -1), timer_func, index);
  else
    luaL_unref(L, -2, index);
  lua_pop(L, 2);
}

static int Lglut_TimerFunc(lua_State *L) {
  unsigned msec = (unsigned)luaL_checkint(L, 1);
  int index;
  lua_rawgetp(L, LUA_REGISTRYINDEX, TIMERTABLE);
  if (lua_isfunction(L, 2)) {
    lua_pushvalue(L, 2);
    index = luaL_ref(L, -2);
  }
  else if (lua_isnumber(L, 2)) {
    index = (int)lua_tointeger(L, 2);
    lua_rawgeti(L, -1, index);
    if (lua_isnil(L, -1)) {
      lua_pushfstring(L, "invalid timer index (%d)", index);
      luaL_argerror(L, 2, lua_tostring(L, -1));
    }
  }
  else {
      lua_pushfstring(L, "function/number expected, got %s", luaL_typename(L, 2));
      return luaL_argerror(L, 2, lua_tostring(L, -1));
  }
  glutTimerFunc(msec, timer_func, index);
  lua_pushinteger(L, index);
  return 1;
}

static int Lglut_CancelTimer(lua_State *L) {
  int index = luaL_checkint(L, 1);
  lua_rawgetp(L, LUA_REGISTRYINDEX, TIMERTABLE);
  luaL_unref(L, -1, index);
  return 0;
}

/* end_spec */


/* !!glutfuncs */

#if defined(USE_OPENGLUT) || defined(USE_OPENGLEAN) || defined(USE_FREEGLUT)
/* !!glutextfuncs */
#endif



#if defined(USE_OPENGLUT) || defined(USE_OPENGLEAN)
/* !!openglutfuncs */
#endif



static void init_libs(lua_State *L) {
  luaL_Reg libs[] = {
#define ENTRY(name) { #name, Lglut_##name }
/* !!glutlibs */
    ENTRY(CancelTimer),

#if defined(USE_OPENGLUT) || defined(USE_OPENGLEAN) || defined(USE_FREEGLUT)
/* !!glutextlibs */
#endif

#if defined(USE_OPENGLUT) || defined(USE_OPENGLEAN)
/* !!openglutlibs */
#endif

#undef  ENTRY
    { NULL, NULL }
  };
  luaL_newlib(L, libs);
}

static void init_constants(lua_State *L) {
  struct { const char *name; int value; } constants[] = {
#define CONSTANT(name) { #name, GLUT_##name }
#if !defined(USE_OPENGLEAN)
    CONSTANT(API_VERSION),
#endif
    /* CONSTANT(XLIB_IMPLEMENTATION) not in freeglut <= 2.2.0 */
    CONSTANT(RGB),
    CONSTANT(RGBA),
    CONSTANT(INDEX),
    CONSTANT(SINGLE),
    CONSTANT(DOUBLE),
    CONSTANT(ACCUM),
    CONSTANT(ALPHA),
    CONSTANT(DEPTH),
    CONSTANT(STENCIL),
    CONSTANT(MULTISAMPLE),
    CONSTANT(STEREO),
    CONSTANT(LUMINANCE),
    CONSTANT(LEFT_BUTTON),
    CONSTANT(MIDDLE_BUTTON),
    CONSTANT(RIGHT_BUTTON),
    CONSTANT(DOWN),
    CONSTANT(UP),
    CONSTANT(KEY_F1),
    CONSTANT(KEY_F2),
    CONSTANT(KEY_F3),
    CONSTANT(KEY_F4),
    CONSTANT(KEY_F5),
    CONSTANT(KEY_F6),
    CONSTANT(KEY_F7),
    CONSTANT(KEY_F8),
    CONSTANT(KEY_F9),
    CONSTANT(KEY_F10),
    CONSTANT(KEY_F11),
    CONSTANT(KEY_F12),
    CONSTANT(KEY_LEFT),
    CONSTANT(KEY_UP),
    CONSTANT(KEY_RIGHT),
    CONSTANT(KEY_DOWN),
    CONSTANT(KEY_PAGE_UP),
    CONSTANT(KEY_PAGE_DOWN),
    CONSTANT(KEY_HOME),
    CONSTANT(KEY_END),
    CONSTANT(KEY_INSERT),
    CONSTANT(LEFT),
    CONSTANT(ENTERED),
    CONSTANT(MENU_NOT_IN_USE),
    CONSTANT(MENU_IN_USE),
    CONSTANT(NOT_VISIBLE),
    CONSTANT(VISIBLE),
    CONSTANT(HIDDEN),
    CONSTANT(FULLY_RETAINED),
    CONSTANT(PARTIALLY_RETAINED),
    CONSTANT(FULLY_COVERED),
    CONSTANT(RED),
    CONSTANT(GREEN),
    CONSTANT(BLUE),
    CONSTANT(NORMAL),
    CONSTANT(OVERLAY),

    CONSTANT(WINDOW_X),
    CONSTANT(WINDOW_Y),
    CONSTANT(WINDOW_WIDTH),
    CONSTANT(WINDOW_HEIGHT),
    CONSTANT(WINDOW_BUFFER_SIZE),
    CONSTANT(WINDOW_STENCIL_SIZE),
    CONSTANT(WINDOW_DEPTH_SIZE),
    CONSTANT(WINDOW_RED_SIZE),
    CONSTANT(WINDOW_GREEN_SIZE),
    CONSTANT(WINDOW_BLUE_SIZE),
    CONSTANT(WINDOW_ALPHA_SIZE),
    CONSTANT(WINDOW_ACCUM_RED_SIZE),
    CONSTANT(WINDOW_ACCUM_GREEN_SIZE),
    CONSTANT(WINDOW_ACCUM_BLUE_SIZE),
    CONSTANT(WINDOW_ACCUM_ALPHA_SIZE),
    CONSTANT(WINDOW_DOUBLEBUFFER),
    CONSTANT(WINDOW_RGBA),
    CONSTANT(WINDOW_PARENT),
    CONSTANT(WINDOW_NUM_CHILDREN),
    CONSTANT(WINDOW_COLORMAP_SIZE),
    CONSTANT(WINDOW_NUM_SAMPLES),
    CONSTANT(WINDOW_STEREO),
    CONSTANT(WINDOW_CURSOR),
    CONSTANT(SCREEN_WIDTH),
    CONSTANT(SCREEN_HEIGHT),
    CONSTANT(SCREEN_WIDTH_MM),
    CONSTANT(SCREEN_HEIGHT_MM),
    CONSTANT(MENU_NUM_ITEMS),
    CONSTANT(DISPLAY_MODE_POSSIBLE),
    CONSTANT(INIT_WINDOW_X),
    CONSTANT(INIT_WINDOW_Y),
    CONSTANT(INIT_WINDOW_WIDTH),
    CONSTANT(INIT_WINDOW_HEIGHT),
    CONSTANT(INIT_DISPLAY_MODE),
    CONSTANT(ELAPSED_TIME),
    CONSTANT(WINDOW_FORMAT_ID),
    CONSTANT(HAS_KEYBOARD),
    CONSTANT(HAS_MOUSE),
    CONSTANT(HAS_SPACEBALL),
    CONSTANT(HAS_DIAL_AND_BUTTON_BOX),
    CONSTANT(HAS_TABLET),
    CONSTANT(NUM_MOUSE_BUTTONS),
    CONSTANT(NUM_SPACEBALL_BUTTONS),
    CONSTANT(NUM_BUTTON_BOX_BUTTONS),
    CONSTANT(NUM_DIALS),
    CONSTANT(NUM_TABLET_BUTTONS),
    CONSTANT(DEVICE_IGNORE_KEY_REPEAT),
    CONSTANT(DEVICE_KEY_REPEAT),
    CONSTANT(HAS_JOYSTICK),
    CONSTANT(OWNS_JOYSTICK),
    CONSTANT(JOYSTICK_BUTTONS),
    CONSTANT(JOYSTICK_AXES),
    CONSTANT(JOYSTICK_POLL_RATE),
    CONSTANT(OVERLAY_POSSIBLE),
    CONSTANT(LAYER_IN_USE),
    CONSTANT(HAS_OVERLAY),
    CONSTANT(TRANSPARENT_INDEX),
    CONSTANT(NORMAL_DAMAGED),
    CONSTANT(OVERLAY_DAMAGED),
    CONSTANT(VIDEO_RESIZE_POSSIBLE),
    CONSTANT(VIDEO_RESIZE_IN_USE),
    CONSTANT(VIDEO_RESIZE_X_DELTA),
    CONSTANT(VIDEO_RESIZE_Y_DELTA),
    CONSTANT(VIDEO_RESIZE_WIDTH_DELTA),
    CONSTANT(VIDEO_RESIZE_HEIGHT_DELTA),
    CONSTANT(VIDEO_RESIZE_X),
    CONSTANT(VIDEO_RESIZE_Y),
    CONSTANT(VIDEO_RESIZE_WIDTH),
    CONSTANT(VIDEO_RESIZE_HEIGHT),
    CONSTANT(ACTIVE_SHIFT),
    CONSTANT(ACTIVE_CTRL),
    CONSTANT(ACTIVE_ALT),
    CONSTANT(CURSOR_RIGHT_ARROW),
    CONSTANT(CURSOR_LEFT_ARROW),
    CONSTANT(CURSOR_INFO),
    CONSTANT(CURSOR_DESTROY),
    CONSTANT(CURSOR_HELP),
    CONSTANT(CURSOR_CYCLE),
    CONSTANT(CURSOR_SPRAY),
    CONSTANT(CURSOR_WAIT),
    CONSTANT(CURSOR_TEXT),
    CONSTANT(CURSOR_CROSSHAIR),
    CONSTANT(CURSOR_UP_DOWN),
    CONSTANT(CURSOR_LEFT_RIGHT),
    CONSTANT(CURSOR_TOP_SIDE),
    CONSTANT(CURSOR_BOTTOM_SIDE),
    CONSTANT(CURSOR_LEFT_SIDE),
    CONSTANT(CURSOR_RIGHT_SIDE),
    CONSTANT(CURSOR_TOP_LEFT_CORNER),
    CONSTANT(CURSOR_TOP_RIGHT_CORNER),
    CONSTANT(CURSOR_BOTTOM_RIGHT_CORNER),
    CONSTANT(CURSOR_BOTTOM_LEFT_CORNER),
    CONSTANT(CURSOR_INHERIT),
    CONSTANT(CURSOR_NONE),
    CONSTANT(CURSOR_FULL_CROSSHAIR),
    CONSTANT(KEY_REPEAT_OFF),
    CONSTANT(KEY_REPEAT_ON),
    CONSTANT(KEY_REPEAT_DEFAULT),
    CONSTANT(JOYSTICK_BUTTON_A),
    CONSTANT(JOYSTICK_BUTTON_B),
    CONSTANT(JOYSTICK_BUTTON_C),
    CONSTANT(JOYSTICK_BUTTON_D),
    CONSTANT(GAME_MODE_ACTIVE),
    CONSTANT(GAME_MODE_POSSIBLE),
    CONSTANT(GAME_MODE_WIDTH),
    CONSTANT(GAME_MODE_HEIGHT),
    CONSTANT(GAME_MODE_PIXEL_DEPTH),
    CONSTANT(GAME_MODE_REFRESH_RATE),
    CONSTANT(GAME_MODE_DISPLAY_CHANGED),

#if defined(USE_OPENGLUT) || defined(USE_OPENGLEAN) || defined(USE_FREEGLUT)
    CONSTANT(ACTION_CONTINUE_EXECUTION),
    CONSTANT(ACTION_EXIT),
    CONSTANT(ACTION_GLUTMAINLOOP_RETURNS),
    CONSTANT(ACTION_ON_WINDOW_CLOSE),
    CONSTANT(CREATE_NEW_CONTEXT),
    CONSTANT(RENDERING_CONTEXT),
    CONSTANT(USE_CURRENT_CONTEXT),
    CONSTANT(VERSION),
    CONSTANT(WINDOW_BORDER_WIDTH),
    CONSTANT(WINDOW_HEADER_HEIGHT),
#endif

#if defined(USE_OPENGLUT) || defined(USE_OPENGLEAN)
    /* experimental features */
    CONSTANT(OFFSCREEN),
    CONSTANT(BORDERLESS),
#endif
#undef CONSTANT
    { NULL, 0 }
  }, *p = constants;
  for (; p->name != NULL; ++p) {
    lua_pushstring(L, p->name);
    lua_pushinteger(L, p->value);
    lua_rawset(L, -3);
  }
}

static void init_pointers(lua_State *L) {
  struct { const char *name; void *value; } pointers[] = {
#define POINTER(name) { #name, GLUT_##name },
    POINTER(STROKE_ROMAN)
    POINTER(STROKE_MONO_ROMAN)
    POINTER(BITMAP_9_BY_15)
    POINTER(BITMAP_8_BY_13)
    POINTER(BITMAP_TIMES_ROMAN_10)
    POINTER(BITMAP_TIMES_ROMAN_24)
    POINTER(BITMAP_HELVETICA_10)
    POINTER(BITMAP_HELVETICA_12)
    POINTER(BITMAP_HELVETICA_18)
#undef POINTER
    { NULL, NULL }
  }, *p = pointers;
  for (; p->name != NULL; ++p) {
    lua_pushstring(L, p->name);
    lua_pushlightuserdata(L, p->value);
    lua_rawset(L, -3);
  }
}

LUALIB_API int luaopen_glut(lua_State *L) {
  ref_L = L;
  init_tables(L);
  init_libs(L);
  init_constants(L);
  init_pointers(L);
  lua_pushstring(L, VERSION);
  lua_setfield(L, -2, "_VERSION");
  return 1;
}
/* cc: flags+='-s -O2 -mdll -DLUA_BUILD_AS_DLL' libs+='-llua52'
 * cc: output='glut.dll'
 * cc: libs+='-lfreeglut32 -lopengl32 -lgdi32 -lwinmm'
 */
