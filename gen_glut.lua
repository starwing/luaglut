local apis = {
-- ======================================================================
-- This is the GLUT API
-- ======================================================================
-- from http://www.opengl.org/resources/libraries/glut/spec3/spec3.html
{
  title = "2. Initialization";
  { "Init", "SPEC" },
  { "InitWindowSize", "vii" },
  { "InitWindowPosition", "vii" },
  { "InitDisplayMode", "vu" },
};
{
  title = "3. Event Processing";
  { "MainLoop", "v" },
};
{
  title = "4. Window Management";
  { "CreateWindow", "SPEC" },
  { "CreateSubWindow", "SPEC" },
  { "SetWindow", "vi" },
  { "GetWindow", "i" },
  { "DestroyWindow", "SPEC" },
  { "PostRedisplay", "v" },
  { "SwapBuffers", "v" },
  { "PositionWindow", "vii" },
  { "ReshapeWindow", "vii" },
  { "FullScreen", "v" },
  { "PopWindow", "v" },
  { "PushWindow", "v" },
  { "ShowWindow", "v" },
  { "HideWindow", "v" },
  { "IconifyWindow", "v" },
  { "SetWindowTitle", "vs" },
  { "SetIconTitle", "vs" },
  { "SetCursor", "vi" },
};
{
  title = "5. Overlay Management";
  { "EstablishOverlay", "v" },
  { "UseLayer", "vE" },
  { "RemoveOverlay", "v" },
  { "PostOverlayRedisplay", "v" },
  { "ShowOverlay", "v" },
  { "HideOverlay", "v" },
};
{
  title = "6. Menu Management";
  { "CreateMenu", "SPEC" },
  { "SetMenu", "vi" },
  { "GetMenu", "i" },
  { "DestroyMenu", "SPEC" },
  { "AddMenuEntry", "vsi" },
  { "AddSubMenu", "vsi" },
  { "ChangeToMenuEntry", "visi" },
  { "ChangeToSubMenu", "visi" },
  { "RemoveMenuItem", "vi" },
  { "AttachMenu", "vi" },
  { "DetachMenu", "vi" },
};
{
  title = "7. Callback Registration";
  { "DisplayFunc", "SPEC" }, 
  { "OverlayDisplayFunc", "SPEC" }, 
  { "ReshapeFunc", "SPEC" }, 
  { "KeyboardFunc", "SPEC" }, 
  { "MouseFunc", "SPEC" }, 
  { "MotionFunc", "SPEC" }, 
  { "PassiveMotionFunc", "SPEC" }, 
  { "SpecialFunc", "SPEC" }, 
  { "IdleFunc", "SPEC" }, 
  { "TimerFunc", "SPEC" }, 
  -- /* implemented [+]:
  --     + glutDisplayFunc
  --     + glutOverlayDisplayFunc
  --     + glutReshapeFunc
  --     + glutKeyboardFunc
  --     + glutMouseFunc
  --     + glutMotionFunc
  --     + glutPassiveMotionFunc
  --       glutVisibilityFunc
  --       glutEntryFunc
  --     + glutSpecialFunc
  --       glutSpaceballMotionFunc
  --       glutSpaceballRotateFunc
  --       glutSpaceballButtonFunc
  --       glutButtonBoxFunc
  --       glutDialsFunc
  --       glutTabletMotionFunc
  --       glutTabletButtonFunc
  --       glutMenuStatusFunc
  --     + glutIdleFunc
  --     + glutTimerFunc */
};
{
  title = "8. Colormap Management";
  { "SetColor", "viFFF" },
  { "GetColor", "Fii" },
  { "CopyColormap", "vi" },
};
{
  title = "9. State Retrieval";
  { "Get", "iE" },
  { "LayerGet", "iE" },
  { "DeviceGet", "iE" },
  { "GetModifiers", "i" },
  { "ExtensionSupported", "is" },
};
{
  title = "10. Font Rendering";
  { "BitmapCharacter", "vpi" },
  { "BitmapWidth", "ipi" },
  { "StrokeCharacter", "vpi" },
  { "StrokeWidth", "ipi" },
};
{
  title = "11. Geometric Object Rendering";
  { "SolidSphere", "vDII" },
  { "WireSphere", "vDII" },
  { "SolidCube", "vD" },
  { "WireCube", "vD" },
  { "SolidCone", "vDDII" },
  { "WireCone", "vDDII" },
  { "SolidTorus", "vDDII" },
  { "WireTorus", "vDDII" },
  { "SolidDodecahedron", "v" },
  { "WireDodecahedron", "v" },
  { "SolidOctahedron", "v" },
  { "WireOctahedron", "v" },
  { "SolidTetrahedron", "v" },
  { "WireTetrahedron", "v" },
  { "SolidIcosahedron", "v" },
  { "WireIcosahedron", "v" },
  { "SolidTeapot", "vD" },
  { "WireTeapot", "vD" },
};
-- End of Functions
}

local extapis = {
  { "BitmapHeight", "ip" },
  { "BitmapString", "vps" },
  { "StrokeHeight", "ip" },
  { "StrokeString", "vps" },

  { "GetMenuData", "p" },
  { "SetMenuData", "vp" },

  { "GetProcAddress", "ps" },

  { "GetWindowData", "p" },
  { "SetWindowData", "vp" },

  { "LeaveMainLoop", "v" },
  { "MainLoopEvent", "v" },

  { "MenuDestroyFunc", "SPEC" },
  { "CloseFunc", "SPEC" },
  { "WMCloseFunc", "SPEC" },
  { "MouseWheelFunc", "SPEC" },
  { "SetOption", "SPEC" },

  { "WireCylinder", "vD2I2" },
  { "WireRhombicDodecahedron", "v" },
  { "SolidCylinder", "vD2I2" },
  { "SolidRhombicDodecahedron", "v" },
}

local openglutapis = {
  { "CreateMenuWindow", "SPEC" },
  { "SetWindowStayOnTop", "vI" },
}

local templates = {
    vii   = "int arg1 = luaL_checkint(L, 1);\n"..
            "int arg2 = luaL_checkint(L, 2);\n"..
            "func(arg1, arg2);\nreturn 0;";
    vpi   = "void *arg1 = lua_touserdata(L, 1);\n"..
            "int arg2 = luaL_checkint(L, 2);\n"..
            "luaL_checktype(L, 1, LUA_TLIGHTUSERDATA);\n"..
            "func(arg1, arg2);\nreturn 0;";
    ipi   = "void *arg1 = lua_touserdata(L, 1);\n"..
            "int arg2 = luaL_checkint(L, 2);\n"..
            "luaL_checktype(L, 1, LUA_TLIGHTUSERDATA);\n"..
            "lua_pushinteger(L, (lua_Integer)func(arg1, arg2));\nreturn 1;";
    vu    = "unsigned int arg1 = luaL_checkunsigned(L, 1);\n"..
            "func(arg1);\nreturn 0;";
    v     = "func();\nreturn 0;";
    vi    = "int arg1 = luaL_checkint(L, 1);\nfunc(arg1);\nreturn 0;";
    i     = "lua_pushinteger(L, (lua_Integer)func());\nreturn 1;";
    vs    = "const char *arg1 = luaL_checkstring(L, 1);\n"..
            "func((char*)arg1);\nreturn 0;";
    vsi   = "const char *arg1 = luaL_checkstring(L, 1);\n"..
            "int arg2 = luaL_checkint(L, 2);\n"..
            "func((char*)arg1, arg2);\nreturn 0;";
    visi  = "int arg1 = luaL_checkint(L, 1);\n"..
            "const char *arg2 = luaL_checkstring(L, 2);\n"..
            "int arg3 = luaL_checkint(L, 3);\n"..
            "func(arg1, (char*)arg2, arg3);\nreturn 0;";
    viFFF = "int arg1 = luaL_checkint(L, 1);\n"..
            "GLfloat arg2 = luaL_checknumber(L, 2);\n"..
            "GLfloat arg3 = luaL_checknumber(L, 3);\n"..
            "GLfloat arg4 = luaL_checknumber(L, 4);\n"..
            "func(arg1, arg2, arg3, arg4);\nreturn 0;";
    Fii   = "int arg1 = luaL_checkint(L, 1);\n"..
            "int arg2 = luaL_checkint(L, 2);\n"..
            "lua_pushnumber(L, (lua_Number)func(arg1, arg2));\nreturn 1;";
    is    = "const char *arg1 = luaL_checkstring(L, 1);\n"..
            "lua_pushinteger(L, (lua_Integer)func((char*)arg1));\nreturn 1;";
    vDII  = "GLdouble arg1 = (GLdouble)luaL_checknumber(L, 1);\n"..
            "GLint arg2 = (GLint)luaL_checkint(L, 2);\n"..
            "GLint arg3 = (GLint)luaL_checkint(L, 3);\n"..
            "func(arg1, arg2, arg3);\nreturn 0;";
    vD    = "GLdouble arg1 = (GLdouble)luaL_checknumber(L, 1);\n"..
            "func(arg1);\nreturn 0;";
    vDDII = "GLdouble arg1 = (GLdouble)luaL_checknumber(L, 1);\n"..
            "GLdouble arg2 = (GLdouble)luaL_checknumber(L, 2);\n"..
            "GLint arg3 = (GLint)luaL_checkint(L, 3);\n"..
            "GLint arg4 = (GLint)luaL_checkint(L, 4);\n"..
            "func(arg1, arg2, arg3, arg4);\nreturn 0;";
    vE    = "GLenum arg1 = luaL_checkint(L, 1);\nfunc(arg1);\nreturn 0;";
    iE    = "GLenum arg1 = luaL_checkint(L, 1);\n"..
            "lua_pushinteger(L, (lua_Integer)func(arg1));\nreturn 1;";
}

local function open_block(title)
  io.write("/*\n** {", ("="):rep(54), "\n", "** ", title, "\n")
  io.write("** ", ("="):rep(55), "\n*/\n\n")
end

local function close_block()
  io.write("/* }", ("="):rep(54), " */\n")
end

local specs = {}
local function gen_binding(func, spec)
  if specs[func] then
    io.write(specs[func])
  elseif templates[spec] then
    local s = templates[spec]:gsub("func", "glut"..func)
    io.write("static int Lglut_", func, "(lua_State *L) {\n")
    io.write("  ", s:gsub("\n", "\n  "), "\n}\n\n")
  end
end
local function gen_entry(funcs)
  for i, v in ipairs(funcs) do
    local func, spec = v[1], v[2]
    io.write("    ENTRY(", func, "),\n")
  end
end

local fields = {
  glutfuncs = function()
    for i, v in ipairs(apis) do
      open_block(v.title)
      for i, v in ipairs(v) do
        gen_binding(v[1], v[2])
      end
      close_block()
      io.write "\n\n"
    end
  end,
  glutextfuncs = function()
    open_block "OpenGLUT/freeGLUT functions"
    for i, v in ipairs(extapis) do
      gen_binding(v[1], v[2])
    end
    close_block()
  end,
  openglutfuncs = function()
    open_block "OpenGLUT functions"
    for i, v in ipairs(openglutapis) do
      gen_binding(v[1], v[2])
    end
    close_block()
  end,
  glutlibs = function()
    for i, v in ipairs(apis) do
      gen_entry(v)
    end
  end,
  glutextlibs = function()
    gen_entry(extapis)
  end,
  openglutlibs = function()
    gen_entry(openglutapis)
  end,
}

io.input  "template.c"
io.output "lglut.c"

local specname, specbody
for line in io.lines() do
  local field = line:match "^%/%* %!%!(%w+) %*%/$"
  if field and fields[field] then
    fields[field]()
  elseif line:match "^%/%* end_spec %*%/$" then
    specs[specname] = specbody
    specname, specbody = nil
  elseif specname then
    local name = line:match "^%/%* begin_spec%((%w+)%) %*%/$"
    if name then
      specs[specname] = specbody
      specname, specbody = name
    elseif not specbody then
      specbody = line .. "\n"
    else
      specbody = specbody .. line .. "\n"
    end
  else
    specname = line:match "^%/%* begin_spec%((%w+)%) %*%/$"
    if not specname then
      io.write(line, "\n")
    end
  end
end
