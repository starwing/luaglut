LUA = lua
CC  = gcc
LD  = gcc
RM  = rm

CFLAGS  = $(MYCFLAGS) -s -O2 -mdll -DLUA_BUILD_AS_DLL $(INC)
LDFLAGS = $(MYLDFLAGS) -s -O2 -mdll
LIBS    = $(MYLIBS) -llua52 -lfreeglut32 -lopengl32 -lgdi32 -lwinmm 

TARGET = glut.dll

all : $(TARGET)

clean :
	-$(RM) lglut.c *.o *.dll

$(TARGET) : lglut.o
	$(LD) $(LDFLAGS) -o $@ $< $(LIBS)

lglut.o : lglut.c
	$(CC) $(CFLAGS) -c -o $@ $<

lglut.c : gen_glut.lua template.c
	$(LUA) gen_glut.lua
